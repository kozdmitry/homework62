import React from 'react';
import {
    AppBar,
    Box,
    Button,
    Grid,
    GridList,
    GridListTile,
    IconButton,
    Link,
    Toolbar,
    Typography
} from "@material-ui/core";
import {classes} from "istanbul-lib-coverage";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Home from "../../components/Pages/Home/Home";
import About from "../../components/Pages/About/About";
import Blog from "../../components/Pages/Blog/Blog";
import Contacts from "../../components/Pages/Contacts/Contacts";
import Gallery from "../../components/Pages/Gallery/Gallery";

const MyPage = () => {
    return (
        <div Container maxWidth="sm" className="Container">
            <AppBar position="static">
                <Toolbar style={{paddingRight: 50}}>
                    <Grid  container
                           direction="row"
                           justify="space-between"
                           alignItems="center">
                        <Typography variant="h6" className={classes.title}>
                            My page
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/" color="inherit">Home</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/about" color="inherit">About</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/blog" color="inherit">Blog</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/contacts" color="inherit">Contacts</Link>
                        </Typography>
                        <Typography className={classes.root}>
                            <Link href="/gallery" color="inherit">Gallery</Link>
                        </Typography>
                    </Grid>

                </Toolbar>
        </AppBar>
            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/blog" component={Blog}/>
                    <Route exact path="/contacts" component={Contacts}/>
                    <Route exact path="/gallery" component={Gallery}/>
                </Switch>
            </Router>
        </div>
    );
};

export default MyPage;